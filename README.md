# prueba-and


Para desplegar la landing page se de proceder de la siguiente manera:

1. Instalar Node.JS  https://nodejs.org/dist/v12.13.0/node-v12.13.0-x64.msi

2. Instalar la libreria de npm server con el siguiente comando:

npm install http-server -g

3. Dirigirse a la ruta la ruta donde se descargo el proyecto de la landing, y colocar el siguiente comando.

http-server -g 

Con ello se abrira el proyecto en el local host http://localhost:8080 

Nota: Si el puerto se encuentra ocupado puede abrirlo en otro puerto con el siguiente comando:

http-server -g -p 8088


